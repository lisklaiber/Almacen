import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PedidoService } from '../services/pedido.service';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'pedido-detalle-dialog',
    templateUrl: '../views/pedido-detalle.dialog.html',
    styleUrls: ['../css/pedido-detalle.dialog.css'],
    providers: [PedidoService]
})
// tslint:disable-next-line:component-class-suffix
export class PedidoDetalleDialog implements OnInit {
    private errorMessage: any;
    public productos: any[];
    constructor(
        public dialogRef: MatDialogRef<PedidoDetalleDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _PedidoService: PedidoService
    ) {
        this.productos = [];
    }

    okClick(): void {
        this.dialogRef.close();
    }

    ngOnInit(): void {
        this.GetProductos();
    }

    GetProductos(): void {
        this._PedidoService.GetProductos(this.data.id).subscribe(response => {
            this.productos = response.resp;
            // console.log(this.productos);
        }, error => {
            this.errorMessage = <any>error;
            if (this.errorMessage != null) {
                // this.openDialog("Ups! No se pudieron cargar los roles", true);
                console.log(this.errorMessage);
                // alert("Usuario no encontrado");
            }
        });
    }

}
