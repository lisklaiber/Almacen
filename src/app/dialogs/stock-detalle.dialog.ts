import { Component, Inject, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'stock-detalle-dialog',
    templateUrl: '../views/stock-detalle.dialog.html',
    styleUrls: ['../css/stock-detalle.dialog.css']
})
// tslint:disable-next-line:component-class-suffix
export class StockDetalleDialog implements OnInit {
    public coordenada: any;
    constructor(
        public dialogRef: MatDialogRef<StockDetalleDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.coordenada = { 'margin-top': '0', 'margin-left': '0' };
    }

    ngOnInit(): void {
        // this.coordenada.top = data''; ['height'].value
    }

    imageload(event, cuadroUbic) {
        let alto = event.target.height;
        let ancho = event.target.width;
        if (ancho > 300) {
            alto = (alto * this.data.y / 100) - 20;
            ancho = (ancho * this.data.x) / 100;
        } else {
            alto = (alto * (this.data.y + 12) / 100) - 30;
            ancho = (ancho * this.data.x + 6) / 100;
            document.getElementById('cuadroUbic').style.height = '20px';
            document.getElementById('cuadroUbic').style.width = '20px';
        }

        this.coordenada['margin-top'] = `${alto}px`;
        this.coordenada['margin-left'] = `${ancho}px`;
        // console.log(`${event.target.width}%`);
        // console.log(this.coordenada);
        // console.log(event);
    }

    okClick(): void {
        this.dialogRef.close();
    }

}
