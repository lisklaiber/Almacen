import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import { ConfigAPI } from '../models/ConfigAPI';
import { Observable } from 'rxjs';

@Injectable()
export class EntregaService {
    private _ConfigAPI: ConfigAPI;
    constructor(private _Http: Http) {
        this._ConfigAPI = new ConfigAPI();
    }

    GetStock(): Observable<any> {
        return this._Http.get(`${this._ConfigAPI.URL}entrega/GetPedidos`).pipe(map(res => res.json()));
    }
}
