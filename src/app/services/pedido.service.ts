import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import { ConfigAPI } from '../models/ConfigAPI';
import { Observable } from 'rxjs';

@Injectable()
export class PedidoService {
    private _ConfigAPI: ConfigAPI;
    constructor(private _Http: Http) {
        this._ConfigAPI = new ConfigAPI();
    }

    GetPendientes(idCliente?: number): Observable<any> {
        if (!idCliente) {
            return this._Http.get(`${this._ConfigAPI.URL}pedido/GetPendientes`).pipe(map(res => res.json()));
        } else {
            return this._Http.get(`${this._ConfigAPI.URL}pedido/GetPendientes/${idCliente}`).pipe(map(res => res.json()));
        }
    }

    GetProductos(idPedido: number): Observable<any> {
        return this._Http.get(`${this._ConfigAPI.URL}pedido/GetProductos/${idPedido}`).pipe(map(res => res.json()));
    }

    GetTiendas(idPedido: number): Observable<any> {
        return this._Http.get(`${this._ConfigAPI.URL}pedido/GetTiendas/${idPedido}`).pipe(map(res => res.json()));
    }
}
