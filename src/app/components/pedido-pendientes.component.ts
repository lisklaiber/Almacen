import { Component, OnInit } from '@angular/core';
import { PedidoService } from '../services/pedido.service';
import { MatDialog } from '@angular/material';
import { PedidoDetalleDialog } from '../dialogs/pedido-detalle.dialog';

@Component({
    selector: 'app-pedido-pendientes',
    templateUrl: '../views/pedido-pendientes.component.html',
    styleUrls: ['../css/pedidos.component.css'],
    providers: [PedidoService]
})

export class PedidoPendientesComponent implements OnInit {

    public pendientes: any[];
    private errorMessage: any;

    constructor(
        private _PedidoService: PedidoService,
        private dialog: MatDialog
    ) {
        this.pendientes = [];
    }

    ngOnInit() {
        this.GetPenditens();
    }

    GetPenditens() {
        this._PedidoService.GetPendientes().subscribe(response => {
            this.pendientes = response.resp;
            // console.log(this.pendientes);
        }, error => {
            this.errorMessage = <any>error;
            if (this.errorMessage != null) {
                // this.openDialog("Ups! No se pudieron cargar los roles", true);
                console.log(this.errorMessage);
                // alert("Usuario no encontrado");
            }
        });
    }

    BtnDetalleClick(pendiente: any) {
        const DIALOGREF = this.dialog.open(PedidoDetalleDialog, {
            data: pendiente
        });

        DIALOGREF.afterClosed().subscribe(result => {
            this.GetPenditens();
        });
    }
}
