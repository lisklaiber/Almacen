import { Component, OnInit } from '@angular/core';
import { StockService } from '../services/stock.service';
import { StockDetalleDialog } from '../dialogs/stock-detalle.dialog';
import { MatDialog } from '@angular/material';

@Component({
    selector: 'app-stock',
    templateUrl: '../views/stock.component.html',
    styleUrls: ['../css/stock.component.css'],
    providers: [StockService]
})

export class StockComponent implements OnInit {
    public productos: any[];
    private errorMessage: any;
    constructor(
        private _StockService: StockService,
        private dialog: MatDialog
    ) {
        this.productos = [];
    }

    ngOnInit() {
        this.GetStock();
    }

    GetStock(): void {
        this._StockService.GetStock().subscribe( response => {
            this.productos = response.resp;
        }, error => {
            this.errorMessage = <any>error;
            if (this.errorMessage != null) {
                // this.openDialog("Ups! No se pudieron cargar los roles", true);
                console.log(this.errorMessage);
                // alert("Usuario no encontrado");
            }
        });
    }

    BtnDetalleClick(producto: any) {
        const DIALOGREF = this.dialog.open(StockDetalleDialog, {
            data: producto
        });

        DIALOGREF.afterClosed().subscribe(result => {
            this.GetStock();
        });
    }
}
