import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PedidoService } from '../services/pedido.service';

@Component({
    selector: 'app-pedido-tiendas',
    templateUrl: '../views/pedido-tiendas.component.html',
    styleUrls: ['../css/pedido-tiendas.component.css'],
    providers: [PedidoService]
})
export class PedidoTiendasComponent implements OnInit {
    private errorMessage: any;
    private idPedido: number;
    private tiendas: any[];
    public codPedido: any[];
    constructor(
        private _route: ActivatedRoute,
        private _PedidoService: PedidoService
    ) { }

    ngOnInit() {
        this._route.params.forEach((params: Params) => {
            this.idPedido = params['idPedido'];
            this.codPedido = params['codPedido'];
            this.GetTiendas(this.idPedido);
        });
    }

    GetTiendas(idPedido: number) {
        this._PedidoService.GetTiendas(idPedido).subscribe(response => {
            this.tiendas = response.resp;
            console.log(this.tiendas);
        }, error => {
            this.errorMessage = <any>error;
            if (this.errorMessage != null) {
                // this.openDialog("Ups! No se pudieron cargar los roles", true);
                console.log(this.errorMessage);
                // alert("Usuario no encontrado");
            }
        });
    }
}
