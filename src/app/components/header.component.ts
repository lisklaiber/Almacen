import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: '../views/header.component.html',
    styleUrls: ['../css/header.component.css']
})
export class HeaderComponent implements OnInit {
    constructor() { }
    ngOnInit() { }
}
