export class ConfigAPI {
    public URL: string;
    public IP: string;
    public LOCAL: string;
    constructor() {
        this.IP = '174.138.0.228';
        this.LOCAL = 'localhost';
        this.URL = `http://${this.LOCAL}:3678/api/`;
    }
}
