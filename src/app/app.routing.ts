import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PedidosComponent } from './components/pedidos.component';
import { StockComponent } from './components/stock.component';
import { PedidoTiendasComponent } from './components/pedido-tiendas.component';
import { PedidoPendientesComponent } from './components/pedido-pendientes.component';

const APPROUTES: Routes = [
    { path: '', component: PedidosComponent },
    { path: 'pedido', component: PedidosComponent, children: [
        { path: 'pendientes', component: PedidoPendientesComponent },
        { path: 'tiendas/:idPedido/:codPedido', component: PedidoTiendasComponent },
        { path: '**', pathMatch: 'full', redirectTo: 'pendientes' }
    ]},
    { path: 'stock', component: StockComponent },
    { path: '**', component: PedidosComponent }
];

export const appRoutingProviders: any[] = [];
export const ROUTING: ModuleWithProviders = RouterModule.forRoot(APPROUTES, { useHash: true });
